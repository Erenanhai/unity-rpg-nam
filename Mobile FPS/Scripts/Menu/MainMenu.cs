using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject levelButtonPrefab;
    public GameObject leverButtonContainer;

    private Transform cameraTransform;
    private Transform cameraLookAtDestination;

    private void Start()
    {
        cameraTransform = Camera.main.transform;

        Sprite[] thumbnails = Resources.LoadAll<Sprite>("Levels");
        foreach(Sprite sprite in thumbnails)
        {
            GameObject container = Instantiate(levelButtonPrefab) as GameObject;
            container.GetComponent<Image>().sprite = sprite;

            container.transform.SetParent(leverButtonContainer.transform, false);

            string sceneName = sprite.name;
            container.GetComponent<Button>().onClick.AddListener(() => LoadLevel(sceneName));

        }
    }

    private void Update()
    {
        if(cameraLookAtDestination != null)
        {
            cameraTransform.rotation = Quaternion.Slerp(cameraTransform.rotation, cameraLookAtDestination.rotation, 3 * Time.deltaTime);
        }
    }

    /// <summary>
    /// Load level
    /// Created by :NamVTP (30/5/2022)
    /// </summary>
    /// <param name="sceneName"></param>
    private void LoadLevel(string sceneName)
    {
        Debug.Log(sceneName);
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// Toggle camera movement to targeted menu screen
    /// Created by :NamVTP (30/5/2022)
    /// </summary>
    /// <param name="menuTransform"></param>
    public void LookAtMenu(Transform menuTransform)
    {
        cameraLookAtDestination = menuTransform;

        //Camera.main.transform.LookAt(menuTransform.position);
    }
}
