using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public GameObject pauseMenu;

    private void Start()
    {
        pauseMenu.SetActive(false);
    }

    /// <summary>
    /// Open pause menu
    /// Created by :NamVTP (30/5/2022)
    /// </summary>
    public void TogglePauseMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }

    /// <summary>
    /// Go back to main menu screen
    /// Created by :NamVTP (30/5/2022)
    /// </summary>
    public void ToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// Reload the game scene
    /// Created by :NamVTP (30/5/2022)
    /// </summary>
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
