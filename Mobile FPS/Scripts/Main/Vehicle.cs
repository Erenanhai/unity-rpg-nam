using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : Interactable
{
    public bool playerOnBoard;
    public Transform dropOffPoint;
    public GameObject vehicleCamera;

    public GameObject player;

    public void EnterVehicle()
    {
        playerOnBoard = true;

        player.transform.parent = dropOffPoint;
        player.SetActive(false);
        player.transform.localPosition = Vector3.zero;

        vehicleCamera.SetActive(true);

        // Control vehicle
    }

    public void ExitVehicle()
    {
        playerOnBoard = false;

        player.transform.parent = null;
        player.SetActive(true);

        // Deactivate vehicle controller

        vehicleCamera.SetActive(false);

        //PlayerController.instance.ResetInput();
    }

    public override void OnInteraction()
    {
        if (!playerOnBoard)
            EnterVehicle();
        else
            ExitVehicle();
    }
}
