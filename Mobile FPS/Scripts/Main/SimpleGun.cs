using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleGun : MonoBehaviour
{
    [Header("Animator")]
    public Animator anim;
    public string animatorParam = "aiming";

    [Header("Camera")]
    public Camera cam;
    public float defaultFOV, aimingFOV;

    [Header("General")]
    public float aimTransitionDuration = 0.2f;

    [Header("Scope")]
    public bool enableScope;
    public MeshRenderer weaponRenderer;
    public GameObject scopeOverlay;

    // Start is called before the first frame update
    void Start()
    {
        if (!weaponRenderer || !scopeOverlay)
            enableScope = false;
    }

    /// <summary>
    /// Handle aiming
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <param name="isAiming"></param>
    /// <returns></returns>
    private IEnumerator Aim(bool isAiming)
    {
        float blendValue = 0, timeElapsed = 0;

        if (enableScope)
        {
            weaponRenderer.enabled = true;
            scopeOverlay.SetActive(false);
        }

        while (timeElapsed < aimTransitionDuration)
        {
            blendValue = timeElapsed / aimTransitionDuration;

            if (isAiming)
            {
                anim.SetFloat(animatorParam, blendValue);
                cam.fieldOfView = Mathf.Lerp(aimingFOV, defaultFOV, 1 - blendValue);
            }
            else
            {
                anim.SetFloat(animatorParam, 1 - blendValue);
                cam.fieldOfView = Mathf.Lerp(aimingFOV, defaultFOV, blendValue);
            }

            timeElapsed += Time.deltaTime;
            yield return null;
        }

        if (enableScope)
        {
            weaponRenderer.enabled = !isAiming;
            scopeOverlay.SetActive(isAiming);
        }

        if (isAiming)
        {
            anim.SetFloat(animatorParam, 1);
            cam.fieldOfView = aimingFOV;
        }
        else
        {
            anim.SetFloat(animatorParam, 0);
            cam.fieldOfView = defaultFOV;
        }
    }

    /// <summary>
    /// Toggle aiming
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <param name="state"></param>
    public void OnAim(bool state)
    {
        StopAllCoroutines();
        StartCoroutine(Aim(state));
    }
}
