using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    public Rigidbody rigidBody;

    public float lifeTime;

    /// <summary>
    /// Spawn bullet with velocity
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <param name="position"></param>
    /// <param name="velocity"></param>
    public void Activate(Vector3 position, Vector3 velocity)
    {
        transform.position = position;
        rigidBody.velocity = velocity;

        gameObject.SetActive(true);

        StartCoroutine("Decay");
    }

    /// <summary>
    /// Despawn bullet
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    public void Deactivate()
    {
        BulletPool.main.AddToPool(this);

        StopAllCoroutines();

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Despawn bullet after certain time
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <returns></returns>
    private IEnumerator Decay()
    {
        yield return new WaitForSeconds(lifeTime);

        Deactivate();
    }

    /// <summary>
    /// Do something when bullet hit
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <param name="other"></param>
    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("A bullet hit something");
        // ...

        Deactivate();
    }
}
