using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CameraBobbing))]
public class PlayerController : MonoBehaviour
{
    public enum PlayerControlMode { FirstPerson, ThirdPerson}
    public PlayerControlMode mode;

    [SerializeField]private CharacterController characterController;
    CameraBobbing cameraBobbing;

    [Header("Gravity & Jumping")]
    public float stickToGroundForce = 10; // for grounded
    public float gravity = 10;            // for airborne
    public float jumpForce = 10;

    private float verticalVelocity;

    [Header("First person camera")]
    public Transform fpCameraTransform;
    [Header("Third person camera")]
    public Transform cameraPole;
    public Transform tpCameraTransform;
    public Transform graphic;

    [Header("Camera Control")]
    Vector2 lookInput;
    float cameraPitch;
    //float cameraYaw;

    [Header("Third person camera setting")]
    public LayerMask cameraObstacleLayers;
    float maxCameraDistance;
    bool isMoving;

    [Header("Player settings")]
    public float cameraSensitivity;
    public float moveSpeed;
    public float moveInputDeadZone;

    [Header("Touch Detection")]
    int leftFingerId, rightFingerId;
    float halfScreenWidth;

    [Header("Player movement")]
    Vector2 moveInput;
    Vector2 moveTouchStartPosition;

    [Header("Ground check")]
    public Transform groundCheck;
    public LayerMask groundLayers;
    public float groundCheckRadius;

    private bool grounded;

    // Start is called before the first frame update
    void Start()
    {
        //touch is not being tracked (initialize)
        leftFingerId = -1;
        rightFingerId = -1;

        halfScreenWidth = Screen.width / 2;

        moveInputDeadZone = Mathf.Pow(Screen.height / moveInputDeadZone, 2);

        cameraBobbing = GetComponent<CameraBobbing>();

        if (mode == PlayerControlMode.ThirdPerson)
        {
            cameraPitch = cameraPole.localRotation.eulerAngles.x;

            maxCameraDistance = tpCameraTransform.localPosition.z;
        }
    }

    // Update is called once per frame
    void Update()
    {
        GetTouchInput();

        if (rightFingerId != -1)
        {
            LookAround();
        }

        if (leftFingerId != -1)
        {
            //Debug.Log("Moving");
            Move();
        }
        else
            cameraBobbing.isWalking = false;

        Fall();
    }

    private void FixedUpdate()
    {
        if (mode == PlayerControlMode.ThirdPerson)
            MoveCamera();

        grounded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundLayers);
    }


    /// <summary>
    /// Track touch inputs in both sides
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <returns></returns>
    private void GetTouchInput()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (touch.position.x < halfScreenWidth && leftFingerId == -1)
                    {
                        leftFingerId = touch.fingerId;
                        //Debug.Log("Tracking left finger...");

                        moveTouchStartPosition = touch.position;
                    }
                    else if (touch.position.x > halfScreenWidth && rightFingerId == -1)
                    {
                        rightFingerId = touch.fingerId;
                        //Debug.Log("Tracking right finger...");
                    }
                    break;

                case TouchPhase.Ended:
                    //break;

                case TouchPhase.Canceled:
                    if (touch.fingerId == leftFingerId)
                    {
                        leftFingerId = -1;
                        //Debug.Log("Stopped tracking left finger.");
                        isMoving = false;
                    }
                    else if (touch.fingerId == rightFingerId)
                    {
                        rightFingerId = -1;
                        //Debug.Log("Stopped tracking right finger.");
                    }
                    break;

                case TouchPhase.Moved:
                    if (touch.fingerId == rightFingerId)
                    {
                        // Get input for looking around
                        lookInput = touch.deltaPosition * cameraSensitivity * Time.deltaTime;
                    }
                    else if (touch.fingerId == leftFingerId)
                    {

                        // calculating the position delta from the start position
                        moveInput = touch.position - moveTouchStartPosition;
                    }
                    break;

                case TouchPhase.Stationary:
                    if (touch.fingerId == rightFingerId)
                    {
                        // Set input to stop looking around (zero)
                        lookInput = Vector2.zero;
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Handle horizontal movement
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <returns></returns>
    private void Move()
    {
        // Don't move if the touch delta is shorter than the designated dead zone
        if (moveInput.sqrMagnitude <= moveInputDeadZone)
        {
            cameraBobbing.isWalking = false;
            isMoving = false;
            return;
        }

        cameraBobbing.isWalking = true;

        if (!isMoving)
        {
            graphic.localRotation = Quaternion.Euler(0, 0, 0);
            isMoving = true;
        }

        // Multiply the normalized direction by the speed
        Vector2 movementDirection = moveInput.normalized * moveSpeed * Time.deltaTime;
        // Move relatively to the local transform's direction
        characterController.Move(transform.right * movementDirection.x + transform.forward * movementDirection.y);
    }

    /// <summary>
    /// Handle gravity and falling
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <returns></returns>
    private void Fall() 
    {
        // Calculate y movement
        if (grounded && verticalVelocity <= 0)
            verticalVelocity = -stickToGroundForce * Time.deltaTime;
        else
            verticalVelocity -= gravity * Time.deltaTime;

        // Apply y movement
        Vector3 verticalMovement = transform.up * verticalVelocity;
        characterController.Move(verticalMovement * Time.deltaTime);
    }

    /// <summary>
    /// Handle jumping
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <returns></returns>
    public void Jump()
    {
        if (grounded)
            verticalVelocity = jumpForce;
    }

    /// <summary>
    /// Handle looking
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <returns></returns>
    private void LookAround()
    {
        // Vertical rotation
        switch (mode)
        {
            case PlayerControlMode.FirstPerson:
                cameraPitch = Mathf.Clamp(cameraPitch - lookInput.y, -90f, 90f);
                fpCameraTransform.localRotation = Quaternion.Euler(cameraPitch, 0, 0);
                break;
            case PlayerControlMode.ThirdPerson:
                cameraPitch = Mathf.Clamp(cameraPitch - lookInput.y, -60f, 90f);
                cameraPole.localRotation = Quaternion.Euler(cameraPitch, 0, 0);
                break;
        }

        if (mode == PlayerControlMode.ThirdPerson && !isMoving)
        {
            // Rotate graphics the opposite way when stationary
            graphic.Rotate(graphic.up, -lookInput.x);
        }

        // Horizontal rotation
        transform.Rotate(transform.up, lookInput.x);
    }

    /// <summary>
    /// Handle third person camera movement
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <returns></returns>
    void MoveCamera()
    {
        Vector3 rayDirection = tpCameraTransform.position - cameraPole.position;
        RaycastHit hit;

        Debug.DrawRay(cameraPole.position, rayDirection, Color.red);
        if (Physics.Raycast(cameraPole.position, rayDirection, out hit, Mathf.Abs(maxCameraDistance), cameraObstacleLayers))
        {
            tpCameraTransform.position = hit.point;
        }
        else
        {
            tpCameraTransform.localPosition = new Vector3(0, 0, maxCameraDistance);
        }
    }
}
