using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteractionController : MonoBehaviour
{
    [Header("Interaction settings")]
    public float maxDistance = 5;
    public LayerMask interactableLayers;

    [Header("UI")]
    public Button interactButton;

    private Interactable currentInteractable;

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, maxDistance, interactableLayers))
        {
            currentInteractable = hit.collider.GetComponent<Interactable>();
        }
        else
        {
            currentInteractable = null;
        }

        interactButton.interactable = currentInteractable != null;
    }

    public void Interact()
    {
        if (currentInteractable)
            currentInteractable.OnInteraction();
    }
}
