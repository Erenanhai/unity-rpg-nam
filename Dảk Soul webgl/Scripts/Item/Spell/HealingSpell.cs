using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BudgetDarkSouls
{
    [CreateAssetMenu(menuName = "Spells/Healing Spell")]
    public class HealingSpell : SpellItem
    {
        public int healAmount;

        public override void AttempToCastSpell(
            AnimatorHandler animatorHandler, 
            PlayerStats playerStats, 
            WeaponSlotManager weaponSlotManager)
        {
            GameObject instantiatedWarmUpSpellFX = Instantiate(spellWarmUpFX, animatorHandler.transform);
            animatorHandler.PlayTargetAnimation(spellAnimation, true);

            Debug.Log("Attemp to heal...");
        }

        public override void SuccessfullyCastSpell(
            AnimatorHandler animatorHandler, 
            PlayerStats playerStats,
            CameraHandler cameraHandler,
            WeaponSlotManager weaponSlotManager)
        {
            GameObject instantiatedSpellFX = Instantiate(spellCastFX, animatorHandler.transform);

            //playerStats.currentHealth = playerStats.currentHealth + healAmount;

            playerStats.HealPlayer(healAmount);

            Debug.Log("Healed");
        }
    }
}