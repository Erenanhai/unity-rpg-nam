using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BudgetDarkSouls
{
    public class DestroyAfterCastingSpell : MonoBehaviour
    {
        CharacterManager playerCastingSpell;

        private void Awake()
        {
            playerCastingSpell = GetComponentInParent<CharacterManager>();
        }
        private void Update()
        {
            if (playerCastingSpell.isFiringSpell)
            {
                Destroy(gameObject);
            }
        }
    }
}
