using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BudgetDarkSouls
{
    public class SpellItem : Item
    {
        public GameObject spellWarmUpFX;
        public GameObject spellCastFX;
        public string spellAnimation;

        [Header("Spell Type")]
        public bool isFaithSpell;
        public bool isMagicSpell;
        public bool isPyroSpell;
        public bool isCryoSpell;

        [Header("Spell Description")]
        [TextArea]
        public string spellDescription;

        public virtual void AttempToCastSpell(
            AnimatorHandler animatorHandler, 
            PlayerStats playerStats, 
            WeaponSlotManager weaponSlotManager)
        {
            Debug.Log("Attemp to cast a spell...");
        }

        public virtual void SuccessfullyCastSpell(
            AnimatorHandler animatorHandler, 
            PlayerStats playerStats, 
            CameraHandler cameraHandler,
            WeaponSlotManager weaponSlotManager)
        {
            Debug.Log("Casting successfully.");
        }
    }
}
