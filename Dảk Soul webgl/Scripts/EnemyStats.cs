using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BudgetDarkSouls
{
    public class EnemyStats : CharacterStats
    {
        Animator animator;

        //[SerializeField] Material material;

        private void Awake()
        {
            animator = GetComponentInChildren<Animator>();
        }

        void Start()
        {
            maxHealth = SetMaxHealthFromHealthLevel();
            currentHealth = maxHealth;

        //    material.SetFloat("dissolveValue", 0);
        }

        private int SetMaxHealthFromHealthLevel()
        {
            maxHealth = healthLevel * 10;
            return maxHealth;
        }

        public override void TakeDamage(int damage, string damageAnimation = "Damage")
        {
            currentHealth = currentHealth - damage;

            animator.Play(damageAnimation);

            if (currentHealth <= 0)
            {
                currentHealth = 0;
                animator.Play("Death");
                // rip
                isDead = true;
                if (isDead)
                {
                    Destroy(gameObject, 6f);
                    return;
                }
            }
        }
    }
}
