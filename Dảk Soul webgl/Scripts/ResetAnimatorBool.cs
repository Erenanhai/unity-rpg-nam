using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetAnimatorBool : StateMachineBehaviour
{
    public string isInteractingBool = "isInteracting";
    public bool isInteractingstatus = false;

    public string isFiringSpellBool = "isFiringSpell";
    public bool isFiringSpellstatus = false;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(isInteractingBool, isInteractingstatus);
        animator.SetBool(isFiringSpellBool, isFiringSpellstatus);
    }
}
