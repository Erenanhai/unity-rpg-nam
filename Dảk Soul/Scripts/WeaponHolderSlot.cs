using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BudgetDarkSouls
{
    public class WeaponHolderSlot : MonoBehaviour
    {
        public Transform parentOverride;
        public bool isLeftHandSlot;
        public bool isRightHandSlot;

        public GameObject currentWeaponModel;

        /// <summary>
        /// Unload weapon
        /// Created by :NamVTP (17/5/2022)
        /// </summary>
        /// <returns></returns>
        public void UnloadWeapon()
        {
            if(currentWeaponModel != null)
            {
                currentWeaponModel.SetActive(false);
            }
        }

        /// <summary>
        /// Unload and destroy weapon
        /// Created by :NamVTP (17/5/2022)
        /// </summary>
        /// <returns></returns>
        public void UnloadWeaponAndDestroy()
        {
            if(currentWeaponModel != null)
            {
                Destroy(currentWeaponModel);
            }
        }

        /// <summary>
        /// Load new weapon
        /// Created by :NamVTP (17/5/2022)
        /// </summary>
        /// <param name="weaponItem"></param>
        /// <returns></returns>
        public void LoadWeaponModel(WeaponItem weaponItem)
        {
            //Unload and Destroy
            UnloadWeaponAndDestroy();

            if(weaponItem == null)
            {
                //Unload weapon
                UnloadWeapon();
                return;
            }

            GameObject model = Instantiate(weaponItem.modelPrefab) as GameObject;
            if(model != null)
            {
                if(parentOverride != null)
                {
                    model.transform.parent = parentOverride;
                }
                else
                {
                    model.transform.parent = transform;
                }

                model.transform.localPosition = Vector3.zero;
                model.transform.localRotation = Quaternion.identity;
                model.transform.localScale = Vector3.one;
            }

            currentWeaponModel = model;
        }
    }
}
