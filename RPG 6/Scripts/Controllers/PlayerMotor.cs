using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerMotor : MonoBehaviour
{
    NavMeshAgent agent;
    Transform target;
    float lookRotationSpeed = 5f;

    /// <summary>
    /// Move to the clicked point
    /// Created by :NamVTP (7/5/2022)
    /// </summary>
    /// <returns></returns>
    public void MoveToPoint(Vector3 point)
    {
        agent.SetDestination(point);
    }

    /// <summary>
    /// Set to follow the interactable
    /// Created by :NamVTP (7/5/2022)
    /// </summary>
    /// <returns></returns>
    public void FollowTarget(Interactable newTarget)
    {
        agent.stoppingDistance = newTarget.radius * .8f;
        agent.updateRotation = false;

        target = newTarget.interactionTransform;
    }

    /// <summary>
    /// Set to stop following the interactable
    /// Created by :NamVTP (7/5/2022)
    /// </summary>
    /// <returns></returns>
    public void StopFollowTarget()
    {
        agent.stoppingDistance = 0f;
        agent.updateRotation = true;

        target = null;
    }

    /// <summary>
    /// Set to rotate toward the interactable
    /// Created by :NamVTP (7/5/2022)
    /// </summary>
    /// <returns></returns>
    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * lookRotationSpeed);
    }


    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            agent.SetDestination(target.position);
            FaceTarget();
        }
    }
}
