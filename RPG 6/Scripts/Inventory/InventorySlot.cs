using UnityEngine;
using UnityEngine.UI;

/* Sits on all InventorySlots. */

public class InventorySlot : MonoBehaviour {

	public Image icon;			// Reference to the Icon image
	public Button removeButton;	// Reference to the remove button

	Item item;  // Current item in the slot

	// <summary>
	/// Add item to the slot
	/// Created by :NamVTP (13/5/2022)
	/// </summary>
	/// <returns></returns>
	public void AddItem (Item newItem)
	{
		item = newItem;

		icon.sprite = item.icon;
		icon.enabled = true;
		removeButton.interactable = true;
	}

	// <summary>
	/// Clear the slot
	/// Created by :NamVTP (13/5/2022)
	/// </summary>
	/// <returns></returns>
	public void ClearSlot ()
	{
		item = null;

		icon.sprite = null;
		icon.enabled = false;
		removeButton.interactable = false;
	}

	// <summary>
	/// Called when the remove button is pressed
	/// Created by :NamVTP (13/5/2022)
	/// </summary>
	/// <returns></returns>
	public void OnRemoveButton ()
	{
		Inventory.instance.Remove(item);
	}

	// <summary>
	/// Called when the item is pressed
	/// Created by :NamVTP (13/5/2022)
	/// </summary>
	/// <returns></returns>
	public void UseItem ()
	{
		if (item != null)
		{
			item.Use();
		}
	}

}
