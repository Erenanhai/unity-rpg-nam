using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Keeps track of enemy stats, loosing health and dying. */

public class EnemyStats : CharacterStats {
	
	// <summary>
	/// Make enemy die
	/// Created by :NamVTP (13/5/2022)
	/// </summary>
	/// <returns></returns>
	public override void Die()
	{
		base.Die();

		// Add ragdoll effect / death animation

		Destroy(gameObject);
	}
}
